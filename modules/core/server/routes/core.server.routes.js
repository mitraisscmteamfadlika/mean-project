
'use strict';

module.exports = function(app) {
  var categories = require('../controllers/categories.server.controller');

  app.route('/categories')
    .get(categories.list)
    .post(categories.create);

  // the categoryId param is added to the params object for the request
  app.route('/categories/:categoryId')
    .get(categories.read)
    .put(categories.update)
    .delete(categories.delete);

  // Finish by binding the article middleware
  // What's this? Where the categoryId is present in the URL
  // the logic to 'get by id' is handled by this single function
  // and added to the request object i.e. request.category.
  // app.param('categoryId', categories.categoryByID);

  // app.route('/categories')
  //   .get(function (request, response) {
  //     response.json([{ name: 'Beverages' }, { name: 'Condiments' }]);
  //   });
};



// Old
// 'use strict';
//
// module.exports = function (app) {
//   // Root routing
//   var core = require('../controllers/core.server.controller');
//
//   // Define error pages
//   app.route('/server-error').get(core.renderServerError);
//
//   // Return a 404 for all undefined api, module or lib routes
//   app.route('/:url(api|modules|lib)/*').get(core.renderNotFound);
//
//   // Define application route
//   app.route('/*').get(core.renderIndex);
// };
